FROM scratch

ADD archlinux-bootstrap.tar.gz /

# Set up pacman
ADD mirrorlist /etc/pacman.d/mirrorlist
RUN pacman-key --init
RUN pacman-key --populate
RUN pacman -Sy

CMD ["/lib/systemd/systemd"]
